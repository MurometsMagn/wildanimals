package org.example.calculator;

import java.math.BigDecimal;

public class EqualityState implements State {
    private Calculator calculator;

    public EqualityState(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void enterNumber(int digit) {
        calculator.setFirstNumber(new BigDecimal(digit));
        calculator.setState(new FirstNumberState(calculator));
        calculator.setDisplayText(calculator.getFirstNumber().toString());
    }

    @Override
    public void enterOperation(char param) {
        calculator.setOperation(param);
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " + param);
        calculator.setState(new OperationState(calculator));
    }

    @Override
    public void enterEquals() throws CalculationException {
        calculator.setSecondNumber(calculator.getFirstNumber());
        //повторяющийся блок:
        calculator.calculate();

        calculator.setDisplayText(calculator.getFirstNumber().toString());
        calculator.setState(new EqualityState(calculator));
    }

    @Override
    public void enterPeriod() throws CalculationException {
        calculator.setState(new FirstNumberState(calculator));
        calculator.setFirstNumber(new BigDecimal(0));
        calculator.setDecimals(1);
    }
}
