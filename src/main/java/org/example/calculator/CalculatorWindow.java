package org.example.calculator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorWindow extends JFrame implements ActionListener {
    private static final String[] buttonName = new String[]
            {"7", "8", "9", "+", "4", "5", "6", "-", "1", "2", "3", "*", "0", ".", "=", "/"};
    private JTextField inputField;
    private Calculator calculator = new Calculator();

    public CalculatorWindow() {
        super("Calculator");
        setSize(800, 600);
        //setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        setUpWindow();
        setVisible(true);
    }

    private void setUpWindow() {

        //add(new JPanel());
        //FlowLayout flowLayout = new FlowLayout();
        //setLayout(flowLayout);
        //panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel(); //buttons
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        //panel2.setFont("Ubuntu Mono", Font.BOLD, 24);
        panel2.setFont(new Font("Ubuntu Mono", Font.BOLD, 24));

        inputField = new JTextField("0");
        inputField.setEditable(false);
        inputField.setFont(new Font("Consolas", Font.PLAIN, 24));
        inputField.setSize(200, 40);
        inputField.setAlignmentY(Component.TOP_ALIGNMENT);
        panel.add(inputField);

        GridLayout gridLayout = new GridLayout(4, 4);
        panel2.setLayout(gridLayout);
        panel.add(panel2);
        for (int i = 0; i < 16; i++) {
            JButton btn = new JButton(buttonName[i]);
            btn.setPreferredSize(new Dimension(50, 50));
            btn.addActionListener(this::actionPerformed);
            panel2.add(btn);
        }

        add(panel);
        pack(); //вычисляет размер окна в зависимости от эл-тов
    }

    public static void main(String[] args) {
        new CalculatorWindow();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand(); //надпись на кнопке

        char tmp = command.charAt(0);
        if (tmp == '=') {
            calculator.enterEquals();
            //inputField.setText(calculator.getDisplayText());
        } else if (tmp == '+' || tmp == '-' || tmp == '/' || tmp == '*') {
            calculator.enterOperation(tmp);
        } else if (tmp == '.') {
            calculator.enterPeriod();
        } else {
            calculator.enterNumber(tmp);
        }
        inputField.setText(calculator.getDisplayText());
    }
}

//webstorm