package org.example.calculator;

public interface State {
    void enterNumber(int digit) throws CalculationException;

    void enterOperation(char param) throws CalculationException;

    void enterEquals() throws CalculationException;

    void enterPeriod() throws CalculationException;
}
