package org.example;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TotalSystem { //singleton
    //все животные
    ArrayList maleHerbivores = new ArrayList();
    ArrayList femaleHerbivores = new ArrayList();
    ArrayList malePredators = new ArrayList();
    ArrayList femalePredators = new ArrayList();

    //сытые животные
    ArrayList fedMalelHerbivores = new ArrayList();
    ArrayList fedFemaleHerbivores = new ArrayList();
    ArrayList fedMalePredators = new ArrayList();
    ArrayList fedFemalePredators = new ArrayList();

    private static TotalSystem totalSystem = new TotalSystem(); //singleton step1of3

    public static TotalSystem getInstance() {//singleton step2of3
        return totalSystem;
    }

    private TotalSystem() {//singleton step3of3

        //заменить одной переменной?
        int intMalePredators = 0;
        int intFemalePredators = 0;
        int intMaleHerbivore = 0;
        int intFemaleHerbivore = 0;

        System.out.println("введите количество самцов хищников: ");
        intMalePredators = input();
        for (int i = 0; i <= intMalePredators; i++) {
            malePredators.add(new MalePredator());
        }

        System.out.println("введите количество самок хищников: ");
        intFemalePredators = input();
        for (int i = 0; i <= intFemalePredators; i++) {
            femalePredators.add(new FemalePredator());
        }

        System.out.println("введите количество самцов травоядных: ");
        intMaleHerbivore = input();
        for (int i = 0; i <= intMaleHerbivore; i++) {
            maleHerbivores.add(new MaleHerbivore());
        }

        System.out.println("введите количество самок травоядных: ");
        intFemaleHerbivore = input();
        for (int i = 0; i <= intFemaleHerbivore; i++) {
            femaleHerbivores.add(new FemaleHerbivore());
        }
    }

    static int input() {
        Scanner scanner = new Scanner(System.in);
        int inputNumber = 0;
        while (true) {
            try {
                inputNumber = scanner.nextInt();
                if (inputNumber >= 1) {
                    return inputNumber;
                } else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }
    }
}
