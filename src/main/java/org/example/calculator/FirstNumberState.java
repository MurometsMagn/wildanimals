package org.example.calculator;

import java.math.BigDecimal;

public class FirstNumberState implements State {
    private Calculator calculator;

    public FirstNumberState(Calculator calculator) {
        this.calculator = calculator;
        calculator.setDecimals(0);
    }

    @Override
    public void enterNumber(int digit) {
        if (calculator.getDecimals() == 0) {
            calculator.setFirstNumber(calculator.getFirstNumber().movePointRight(1).add(new BigDecimal(digit)));
        } else {
//            double tmp = (double) digit;
//            for (int i = 0; i < calculator.getDecimals(); i++) {
//                tmp /= 10;
//            }
            BigDecimal tmp = new BigDecimal(digit);
            tmp = tmp.movePointLeft(calculator.getDecimals());

            calculator.setFirstNumber(calculator.getFirstNumber().add(tmp));
            calculator.setDecimals(calculator.getDecimals() + 1);
        }
        calculator.setDisplayText(calculator.getFirstNumber().toString());
    }

    @Override
    public void enterOperation(char param) {
        calculator.setOperation(param);
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " + param);
        calculator.setState(new OperationState(calculator));
    }

    @Override
    public void enterEquals() {
        //do nothing, ignore typing
        calculator.setState(new OperationState(calculator));
    }

    @Override
    public void enterPeriod() throws CalculationException {
        if (calculator.getDecimals() == 0) {
            calculator.setDecimals(1);
        }
    }
}
