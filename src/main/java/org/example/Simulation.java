package org.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Simulation {
    Scanner scanner = new Scanner(System.in);


    //создать экземпляр TotalSystem
    TotalSystem totalSystem = TotalSystem.getInstance();

    //подневная эмуляция
    public void emulate() {
        boolean exit = false;
        //emulation:
        for (int day = 1; !exit; day++) { //пока не будет введено слово Exit
            //

            showDayResults(day);
            exit = askForExit();
        }
    }

    private void showDayResults(int day) {
        System.out.println("Результаты " + day + "-го дня: ");
        System.out.println("Самцов-хищников: " + TotalSystem.getInstance().malePredators.size());
        System.out.println("Самок-хищников: " + TotalSystem.getInstance().femalePredators.size());
        System.out.println("Самцов-травоядных: " + TotalSystem.getInstance().maleHerbivores.size());
        System.out.println("Самок-травоядных: " + TotalSystem.getInstance().femaleHerbivores.size());
    }

    private boolean askForExit() {
        System.out.println("введите Exit чтобы прервать или нажмитe Ввод чтобы продолжить: ");
        String inputData;
        while (true) {
            try {
                inputData = scanner.nextLine(); //в случае нажатия Enter
                if (inputData.equals("exit")) {
                    return true;
                    //continueInput = false;
                } else if (inputData.equals("")) {//как-то можно перехватить exit..
                    return false;
                } else System.out.println("Некоректный ввод, попробуйте еще раз: ");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Something wrong)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }
    }
}
