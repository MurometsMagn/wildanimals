package org.example.calculator;

import java.math.BigDecimal;

public class SecondNumberState implements State {
    private Calculator calculator;

    public SecondNumberState(Calculator calculator) {
        this.calculator = calculator;
        calculator.setDecimals(0);
    }

    @Override
    public void enterNumber(int digit) {
        if (calculator.getDecimals() == 0) {
            calculator.setSecondNumber(calculator.getSecondNumber().movePointRight(1).add(new BigDecimal(digit)));
        } else {
            BigDecimal tmp = new BigDecimal(digit).movePointLeft(calculator.getDecimals());
            //  tmp /= Math.pow(10, calculator.getDecimals());
            calculator.setSecondNumber(calculator.getSecondNumber().add(tmp));
            calculator.setDecimals(calculator.getDecimals() + 1);
        }
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " +
                calculator.getOperation() + " " + calculator.getSecondNumber());
    }

    @Override
    public void enterOperation(char param) throws CalculationException {
        //повторяющийся блок:
        calculator.calculate();

        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " + param);
        calculator.setOperation(param);
        calculator.setState(new OperationState(calculator));
    }

    @Override
    public void enterEquals() throws CalculationException {
        //повторяющийся блок:
        calculator.calculate();

        calculator.setDisplayText(calculator.getFirstNumber().toString());
        calculator.setState(new EqualityState(calculator));
    }

    @Override
    public void enterPeriod() throws CalculationException {
        if (calculator.getDecimals() == 0) {
            calculator.setDecimals(1);
        }
    }
}
