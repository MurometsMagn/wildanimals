package org.example.calculator;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CalculatorTest {
    @Test
    public void testFirstNumberInput() {
        Calculator calculator = new Calculator();
        calculator.enterNumber('1');
        assertEquals("1", calculator.getDisplayText());
        calculator.enterNumber('2');
        assertEquals("12", calculator.getDisplayText());
        calculator.enterPeriod();
        assertEquals("12", calculator.getDisplayText());
        calculator.enterNumber('3');
        assertEquals("12.3", calculator.getDisplayText());
        calculator.enterNumber('4');
        assertEquals("12.34", calculator.getDisplayText());
    }

    @Test
    public void testOperationInput() {
        Calculator calculator = new Calculator(); //лучше каждый раз чистый калькулятор
        calculator.setFirstNumber(new BigDecimal(12));
        calculator.enterOperation('+');
        assertEquals("12 +", calculator.getDisplayText());
        calculator.enterOperation('-');
        assertEquals("12 -", calculator.getDisplayText());

        //2+3-
        calculator.enterNumber('4');
        calculator.enterOperation('-');
        assertEquals("8 -", calculator.getDisplayText());
    }

    @Test
    public void testSecondNumberInput() {
        Calculator calculator = new Calculator();
        calculator.setFirstNumber(new BigDecimal(12));
        calculator.enterOperation('+');
        calculator.enterNumber('3');
        assertEquals("12 + 3", calculator.getDisplayText());
        calculator.enterNumber('7');
        assertEquals("12 + 37", calculator.getDisplayText());
        calculator.enterPeriod();
        assertEquals("12 + 37", calculator.getDisplayText());
        calculator.enterNumber('8');
        assertEquals("12 + 37.8", calculator.getDisplayText());
        calculator.enterNumber('9');
        assertEquals("12 + 37.89", calculator.getDisplayText());
    }

    @Test
    public void testEqualityInput() {
        Calculator calculator = new Calculator();
        //calculator.setFirstNumber(12d); //display is clear
        calculator.enterNumber('3');
        calculator.enterEquals();
        assertEquals("3", calculator.getDisplayText());

        calculator.enterOperation('+');
        calculator.enterEquals(); // 3 + =
        assertEquals("6", calculator.getDisplayText());

        calculator.enterNumber('4'); //ввод числа в соостоянии Equality
        assertEquals("4", calculator.getDisplayText());

//        //предыдущее значение "4.0" остается на экране
//        calculator.setFirstNumber(4.5);
//        calculator.setOperation('-');
//        calculator.setSecondNumber(2.2);
//        calculator.enterEquals();
//        assertEquals("2.3", calculator.getDisplayText()); // падает

        calculator.enterOperation('/');
        calculator.enterNumber('2');
        calculator.enterEquals();
        assertEquals("2", calculator.getDisplayText());
    }

    @Test
    public void testZeroDivision() {
        Calculator calculator = new Calculator();
        calculator.setFirstNumber(new BigDecimal(12));
        calculator.enterOperation('/');
        calculator.enterNumber('0');
        calculator.enterEquals();
        assertEquals(Calculator.zeroDivMessage, calculator.getDisplayText());

        calculator.enterNumber('7');
        calculator.enterOperation('/');
        calculator.enterNumber('0');
        calculator.enterOperation('+');
        assertEquals(Calculator.zeroDivMessage, calculator.getDisplayText());
    }
}
//тесты для ост ситуаций работы с кальк.


//(перевезти на BigDecimals)
//lucidchart.com