package org.example;

import javax.swing.*;
import java.awt.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * If some method is often used in dif projects.
 * by Smolentsev Il'ya
 */

public class MyOwnLib extends JFrame{
    private static Scanner scanner = new Scanner(System.in);

    public static int keyboardInputInt(int minValue, int maxValue) {
        int inputData;
        while (true) {
            try {
                inputData = scanner.nextInt();
                if (inputData >= minValue && inputData <= maxValue)
                    break;
                else
                    System.out.println("некоректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again. (" + "Incorrect input: an integer is required)");
                scanner.nextLine(); //при некорректном вводе -> для перевода курсора с ошибочной позиции
            }
        }
        return inputData;
    }

    public static String keyboardInputString(String requestToUser) {
        String inputData; //responseFromUser
        do {
            System.out.println(requestToUser);
            inputData = scanner.nextLine();
        } while (inputData.trim().isEmpty());
        return inputData;
    }

    public void initGUI(String title) {
        //порядок не менять:
        setTitle(title);
        setSize(800, 600);
        setMinimumSize(new Dimension(300, 200));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //can be replaced
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
