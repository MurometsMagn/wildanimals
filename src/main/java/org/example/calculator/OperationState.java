package org.example.calculator;

import java.math.BigDecimal;

public class OperationState implements State {
    private Calculator calculator;

    public OperationState(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void enterNumber(int digit) {
        calculator.setSecondNumber(new BigDecimal(digit));
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " +
                calculator.getOperation() + " " + digit);
        calculator.setState(new SecondNumberState(calculator));
    }

    @Override
    public void enterOperation(char param) {
        calculator.setOperation(param);
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " + param);
    }

    @Override
    public void enterEquals() throws CalculationException {
        calculator.setSecondNumber(calculator.getFirstNumber());
        //повторяющийся блок:
        calculator.calculate();

        calculator.setDisplayText(calculator.getFirstNumber().toString());
        calculator.setState(new EqualityState(calculator));
    }

    @Override
    public void enterPeriod() throws CalculationException {
        calculator.setState(new SecondNumberState(calculator));
        calculator.setSecondNumber(new BigDecimal(0));
        calculator.setDecimals(1);
    }
}
