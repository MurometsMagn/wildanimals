package org.example.calculator;

import java.math.BigDecimal;

public class ErrorState implements State{
    private Calculator calculator;

    public ErrorState(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void enterNumber(int digit) {
        calculator.setFirstNumber(new BigDecimal(digit));
        calculator.setState(new FirstNumberState(calculator));
        calculator.setDisplayText(calculator.getFirstNumber().toString());
    }

    @Override
    public void enterOperation(char param) {
        calculator.setFirstNumber(new BigDecimal(0));
        calculator.setOperation(param);
        calculator.setDisplayText(calculator.getFirstNumber().toString() + " " + param);
        calculator.setState(new OperationState(calculator));
    }

    @Override
    public void enterEquals() {
        //do nothing
    }

    @Override
    public void enterPeriod() throws CalculationException {
        calculator.setState(new FirstNumberState(calculator));
        calculator.setFirstNumber(new BigDecimal(0));
        calculator.setDecimals(1);
    }
}
