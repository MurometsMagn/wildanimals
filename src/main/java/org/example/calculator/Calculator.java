package org.example.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {
    private String displayText = "0";
    private State state = new FirstNumberState(this);
    private BigDecimal firstNumber = new BigDecimal(0);
    private BigDecimal secondNumber = new BigDecimal(0);
    private char operation; //добавлено
    private int decimals = 0; //количество знаков после запятой
    static final String zeroDivMessage = "деление на ноль";

    public void enterNumber(char param) {
        int number = param - '0'; //преобразование к int
        try {
            state.enterNumber(number);
        } catch (CalculationException e) {
            displayText = e.getMessage();
            state = new ErrorState(this);
        }
    }

    public void enterOperation(char param) {
        try {
            state.enterOperation(param);
        } catch (CalculationException e) {
            displayText = e.getMessage();
            state = new ErrorState(this);
        }
    }

    public void enterEquals() {
        try {
            state.enterEquals();
        } catch (CalculationException e) {
            displayText = e.getMessage();
            state = new ErrorState(this);
        }
    }

    public void enterPeriod() {
        try {
            state.enterPeriod();
        } catch (CalculationException e) {
            displayText = e.getMessage();
            state = new ErrorState(this);
        }
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public BigDecimal getFirstNumber() {
        return firstNumber.stripTrailingZeros();
    }

    public void setFirstNumber(BigDecimal firstNumber) {
        this.firstNumber = firstNumber.setScale(decimals);
    }

    public BigDecimal getSecondNumber() {
        return secondNumber.stripTrailingZeros();
    }

    public void setSecondNumber(BigDecimal secondNumber) {
        this.secondNumber = secondNumber.setScale(decimals); //problem
    }

    public char getOperation() {
        return operation;
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    public int getDecimals() {
        return decimals;
    }

    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void calculate() throws CalculationException { //повторяющийся блок кода
        BigDecimal tmp = new BigDecimal(0);
        if (operation == '+') {
            tmp = firstNumber.add(secondNumber);
        } else if (operation == '-') {
            tmp = firstNumber.subtract(secondNumber);
        } else if (operation == '*') {
            tmp = firstNumber.multiply(secondNumber);
        } else if (operation == '/') {
            if (secondNumber.compareTo(new BigDecimal(0)) == 0) {
//                displayText = "деление на ноль";
//                state = new ErrorState(this);
//                return;
                throw new CalculationException(zeroDivMessage);
            }
            tmp = firstNumber.divide(secondNumber, 8, RoundingMode.HALF_UP);

        }
        firstNumber = tmp;
    }
}

//division

